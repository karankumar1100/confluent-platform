# frozen_string_literal: true

require 'serverspec'

set :backend, :exec

def waiting(service, port, trys = 30)
  trys.times do |try|
    out = `ss -tunl | grep -- :#{port}`
    break unless out.empty?

    puts "Waiting #{service} to launch… (##{try}/#{trys})"
    puts('=' * 80)
    puts `journalctl -u #{service} -e -n 10`
    puts('=' * 80)
    sleep(5)
  end
end
